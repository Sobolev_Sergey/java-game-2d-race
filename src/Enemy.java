package sks.sergey;

import javax.swing.*;
import java.awt.*;

/**
 * Created by SKS on 24.08.2017.
 */
public class Enemy {
    int x;
    int y;
    int v;

    Image img = new ImageIcon("res/enemy.png").getImage();

    Road road;

    public Rectangle getRect(){                                             // столкновения
        return new Rectangle(x, y, 230, 100);
    }

    public Enemy (int x, int y, int v, Road road){
        this.x = x;
        this.y = y;
        this.v = v;
        this.road = road;

    }

    public void move(){
        x = x - road.p.v + v;

    }

}
