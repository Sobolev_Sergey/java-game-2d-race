package sks.sergey;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.*;
import java.util.List;
import javax.swing.Timer;

/**
 * Created by SKS on 23.08.2017.
 */

public class Road extends JPanel implements ActionListener, Runnable {

    Timer mainTimer = new Timer(20, this);                  // таймер каждые 20 мс запускает actionPerformed

    Image img = new ImageIcon("res/road.jpg").getImage();       // загружаем дорогу

    Player p = new Player();                                            // создаем машину

    Thread enemiesFactory = new Thread(this);                    // в новом потоке будут враги

    List<Enemy> enemies = new ArrayList<Enemy>();                        // лист наших врагов

    public Road(){
        mainTimer.start();                                              // запускаем наш таймер
        enemiesFactory.start();                                         // запуск потока врагов
        addKeyListener(new myKeyAdapter());                             // регистрируем слушателя
        setFocusable(true);                                             // делаем фокус на дороге
    }

    @Override
    public void run() {                                                 // точка входа в наш 2й поток

        while (true){
            Random rand = new Random();
            try {
                Thread.sleep(rand.nextInt(2000));                // усыпим поток случайно от 0 до 2000
                enemies.add(new Enemy(1200,
                        rand.nextInt( 460)+250,
                        rand.nextInt(60),
                        this));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private class myKeyAdapter extends KeyAdapter{                      // слушатель клавиатуры
        @Override
        public void keyPressed(KeyEvent e) {                            // когда клавиша нажата
            p.keyPressed(e);
        }
        @Override
        public void keyReleased(KeyEvent e) {                           // когда клавиша освобождена
            p.keyReleased(e);
        }
    }

    public void paint (Graphics g){                                     // перерисовывает дорогу
        g = (Graphics2D) g;                                             // приведение типа
        g.drawImage(img, p.layer1, 0, null);                // что рисуем и где рисуем
        g.drawImage(img, p.layer2, 0, null);                // и рисуем второй слой
        g.drawImage(p.img, p.x, p.y, null);                     // рисуем машину на дороге

        double v = (200/Player.MAX_V) * p.v;
        g.setColor(Color.RED);
        Font font = new Font("Arial", Font.BOLD, 30);
        g.setFont(font);
        g.drawString("Скорость: " + v + "км/ч", 50, 30);

        Iterator<Enemy> i = enemies.iterator();                         // пробегаемся по элементам
        while (i.hasNext()){                                            // пока существует след элемент
            Enemy e = i.next();                                         // получаем новый объект
            if (e.x >=2400 || e.x <= -2400) {                            // если враг далеко за пределами удаляем его
                i.remove();
            } else {
                e.move();
                g.drawImage(e.img, e.x, e.y, null);             // иначе рисуем врага
            }
        }



    }

    @Override
    public void actionPerformed(ActionEvent e) {
        p.move();
        repaint();
        testCollisionWithEnemies();                                     // проверка столкновения
        testWin();                                                      // проверка победы
    }

    private void testWin() {
        if (p.s > 20000){
            JOptionPane.showMessageDialog(null, "Победа!!!");
            System.exit(0);
        }
    }

    private void testCollisionWithEnemies() {                           // метод проверки столкновения
        Iterator<Enemy> i = enemies.iterator();

        while (i.hasNext()){
            Enemy e = i.next();
            if (p.getRect().intersects(e.getRect())){                   // усли прямоугольники столкнулись
                //i.remove();                                             // удаляем
                JOptionPane.showMessageDialog(null, "Авария!!!");
                System.exit(1);
            }
        }

    }
}
