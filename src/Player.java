package sks.sergey;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by SKS on 23.08.2017.
 */

public class Player {                                                   // машина игрока

    public static final int MAX_V = 70;                                 // макс скорость пикс за обновление
    public static final int MAX_TOP = 140;                               // макс координата нашей машины отн верх угла
    public static final int MAX_BOTTOM = 460;                           // макс коорд отн низа

    Image img_c = new ImageIcon("res/player.png").getImage();     // изображение машины
    Image img_l = new ImageIcon("res/playerL.png").getImage();
    Image img_r = new ImageIcon("res/playerR.png").getImage();
    Image img = img_c;

    public Rectangle getRect(){                                         // столкновения
        return new Rectangle(x, y, 200, 100);
    }

    int v = 10;                                                          // начальная скорость машины
    int dv = 0;                                                         // ускорение
    int s = 0;                                                          // пройденный путь

    int x = 50;                                                        // коорд. появления машины
    int y = 250;
    int dy = 0;

    int layer1 = 0;                                                     // координата первого слоя
    int layer2 = 1200;                                                  // координата второго слоя

    public void move() {                                                // метод езды
        s += v;                                                         // накапливаем путь
        v += dv;

        if (v <= 0) v = 0;
        if (v >= MAX_V) v = MAX_V;

        y -= dy;
        if (y <= MAX_TOP) y = MAX_TOP;
        if (y >= MAX_BOTTOM) y = MAX_BOTTOM;

        if (layer2 - v <= 0){                                           // зацикливаем прорисовку дороги
            layer1 = 0;
            layer2 = 1200;
        } else {
            layer1 -= v;                                                // а координаты уменьшились
            layer2 -= v;
        }
    }

    public void keyPressed(KeyEvent e) {                                // реализация метода нажатия клавиши
        //JOptionPane.showMessageDialog(null, "key pressed");

        int key = e.getKeyCode();                                       // получаем код клавиши которую нажали
        if (key == KeyEvent.VK_RIGHT){                                  // нажимаем вправо - ускоряемся
            dv = 1;
        }
        if (key == KeyEvent.VK_LEFT){                                  // нажимаем влево - замедляемся, тормозим
            dv = -1;
        }
        if (key == KeyEvent.VK_UP){                                  // нажимаем вверх - вверх
            dy = 5;
            img = img_l;                                               // поворот на лево
        }
        if (key == KeyEvent.VK_DOWN){                                  // нажимаем вверх - вверх
            dy = -5;
            img = img_r;                                                // поворот на право
        }
    }

    public void keyReleased(KeyEvent e) {                               // реализация метода отпускания клавиши
        //JOptionPane.showMessageDialog(null, "key released");
        int key = e.getKeyCode();                                       // получаем код клавиши которую отпускаем
        if (key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_LEFT){       // отпускаем вправо или лево - ускорение обнуляется
            dv = 0;
        }
        if (key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN){       // отпускаем вверх или вниз - выравниваемся
            dy = 0;
            img = img_c;                                                // отпускаем клавишу - машина опять едет прямо
        }

    }
}
